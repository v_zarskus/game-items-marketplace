import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MockDataService } from 'src/app/services/mock-data.service';
import { startCase } from "lodash";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit, OnDestroy {
  gameName: string;
  normalizedGameName: string;
  itemId: string;
  apiSubscription: any;
  item: any;

  constructor(
    private route: ActivatedRoute,
    private api: MockDataService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.gameName = params.gameName;
      this.itemId = params.id;
      this.normalizedGameName = startCase(this.gameName);
    });
    // Subscribe to the mock API to receive data
    this.apiSubscription = this.api.dbAPI.subscribe(item => {this.item = item; console.log(item)});
    this.api.getItem(this.gameName, this.itemId);
  }

  ngOnDestroy(): void {
    this.apiSubscription.unsubscribe();
  }

}
