import { Component, OnInit } from '@angular/core';
import { KeyValue } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  allGames = {
    'Rocket League': {
      path: 'rocketLeague',
      primaryImg: 'https://assets.eldorado.gg/images/boxImages/desktop/v2/Rocket-League.jpg',
      secondaryImg: 'https://assets.eldorado.gg/images/icons/v2/Rocket-League.png'
    },
    'CSGO': {
      path: 'CSGO',
      primaryImg: 'https://assets.eldorado.gg/images/boxImages/desktop/v2/CSGO.jpg',
      secondaryImg: 'https://assets.eldorado.gg/images/icons/v2/CSGO.png'
    },
    'World of Warcraft': {
      path: 'worldOfWarcraft',
      primaryImg: 'https://assets.eldorado.gg/images/boxImages/desktop/v2/World-of-Warcraft-Classic.jpg',
      secondaryImg: 'https://assets.eldorado.gg/images/icons/v2/World-of-Warcraft-Classic.png'
    },
    'Path of Exile': {
      path: 'pathOfExile',
      primaryImg: 'https://assets.eldorado.gg/images/boxImages/desktop/v2/Path-of-Exile.jpg',
      secondaryImg: 'https://assets.eldorado.gg/images/icons/v2/Path-of-Exile.png'
    },
    'Overwatch': {
      path: 'overwatch',
      primaryImg: 'https://assets.eldorado.gg/images/boxImages/desktop/v2/Overwatch.jpg',
      secondaryImg: 'https://assets.eldorado.gg/images/icons/v2/Overwatch.png'
    },
    'Borderlands 3': {
      path: 'borderlands3',
      primaryImg: 'https://assets.eldorado.gg/images/boxImages/desktop/v2/Borderlands-3.jpg',
      secondaryImg: 'https://assets.eldorado.gg/images/icons/v2/Borderlands-3.png'
    }
  }
  
  constructor() { }

  ngOnInit(): void {
  }

  // Preserve the original property order when iterating through the object with keyvalue pipe
  originalOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
    return 0;
  }

}
