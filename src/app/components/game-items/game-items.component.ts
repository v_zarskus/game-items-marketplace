import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MockDataService } from 'src/app/services/mock-data.service';

@Component({
  selector: 'app-game-items',
  templateUrl: './game-items.component.html',
  styleUrls: ['./game-items.component.scss']
})
export class GameItemsComponent implements OnInit, OnDestroy {
  gameName: string;
  apiSubscription: any;
  allItems: any;
  filteredItems: any;
  filters: any = {};

  constructor(
    private route: ActivatedRoute,
    private api: MockDataService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.gameName = params.gameName);

    // Subscribe to the mock API to receive data
    this.apiSubscription =  this.api.dbAPI.subscribe(items => {
      this.allItems = items;
      this.filteredItems = Array.from(this.allItems);
      // Get all possible filter names and their values
      this.allItems.forEach(item => {
        Object.entries(item.attributes).forEach(([attrName, attrValue]) => {
          if (!this.filters.hasOwnProperty(attrName)) {
            this.filters[attrName] = [];
          };
          if (!this.filters[attrName].includes(attrValue)) {
            this.filters[attrName].push(attrValue)
          }
        })
      })

    });
    // Request items from and "endpoint"
    this.api.getAllItems(this.gameName);
  }

  // Filter by selected attribute or multiple attributes
  addFilter(form): void {
    this.filteredItems = Array.from(this.allItems); // Reset filtered items

    // Apply/remove filters
    Object.entries(form.value).forEach(([filterName, filterValue]) => {
      if (filterValue !== 'ALL') {
        this.filteredItems = this.filteredItems.filter(item => {
          if (item.attributes[filterName] === filterValue) {
            return item
          }
        })
      } else {
        return
      }
    })
    console.log(this.filteredItems, 'filtered items');
  }

  ngOnDestroy(): void {
    this.apiSubscription.unsubscribe()
  }

}
