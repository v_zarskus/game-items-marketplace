import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomnessService {

  constructor() { }

  randomNumber(max: number, min: number) {
    return Math.floor(Math.random() * (max - min +1)) + min
  }

  randomFromArray(array: any[]) {
    return array[Math.floor(Math.random() * array.length)]
  }

  randomItemsFromArray(array: any[], n: number) {
    return array.sort(() => 0.5 - Math.random()).slice(0, n);
  }
}
