import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { RandomnessService } from './randomness.service';
import { Subject } from 'rxjs';
import { v4 as generateUuid } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class MockDataService {
  allGames: any;
  db: any = {};
  dbAPI = new Subject();
  allSellers = ['haidi1408', 'magicseller', 'MaryFan', 'LoL Store Place', 'ingentior', 'sellstuff'];

  constructor(
    private http: HttpClient,
    private random: RandomnessService
    ) { 
    this.http.get('assets/database/db-structure.json').subscribe(data => {
      // Using http module to read JSON file
      this.allGames = data;
      const existingDb = localStorage.getItem('dbForAtlantisGamesTask');
      if (existingDb) {
        this.db = JSON.parse(existingDb);
        console.log(this.db)
      } else {
        this.generateDb();
      }
    })
  }

  // Use the data from db-structure.json to generate a randomized database of game items
  private generateDb(): void {
    Object.entries(this.allGames).forEach(([gameName, gameData]: [string, any]) => {
      this.db[gameName] = [];
      // Generate a random number items for the game
      const numberOfItems = this.random.randomNumber(gameData.minItems, gameData.maxItems);
      // Create the items
      [...Array(numberOfItems)].forEach(() => {
        const item: any = {attributes: {}};
        const attributes = Object.entries(gameData.attributes);
        // Generate random values for each attribute of the item, and image and/or name if
        //they are dependent on an attribute
        attributes.forEach(([attrName, attrValues]: [string, []]) => {
          const attrValue = this.random.randomFromArray(attrValues);
          item.attributes[attrName] = attrValue;
          if (gameData.images.hasOwnProperty(attrName)) {
            item.img = this.random.randomFromArray(gameData.images[attrName][attrValue])
          };
          if (gameData.names.hasOwnProperty(attrName)) {
            item.name = this.random.randomFromArray(gameData.names[attrName][attrValue])
          };
        });
        if (!item.hasOwnProperty('img')) {
          item.img = this.random.randomFromArray(gameData.images);
        };
        if (!item.hasOwnProperty('name')) {
          item.name = this.random.randomFromArray(gameData.names);
        };
        item.id = generateUuid();
        item.sellers = [];
        // Pick a random number of sellers
        const sellers = this.random.randomItemsFromArray(this.allSellers, this.random.randomNumber(1, 6));
        // Add sellers and their info
        sellers.forEach(seller => {
          item.sellers.push({
            name: seller,
            rating: this.random.randomNumber(40, 100),
            timeEstimate: this.random.randomNumber(1, 60),
            price: this.random.randomNumber(1, 10)
          })
        })
        // Sort sellers by rating
        item.sellers.sort((a, b) => b.rating - a.rating);
        this.db[gameName].push(item);
      });

    });
    // Save the mock data to local storage so that new random data is not generated on every refresh
    localStorage.setItem('dbForAtlantisGamesTask', JSON.stringify(this.db));
  }

  // Mock an API call to backend
  getAllItems(gameName: string): void {
    setTimeout(() => {
      this.dbAPI.next(this.db[gameName]);
    }, 1000)
  }

  // Mock an API call to backend
  getItem(gameName: string, id: string): void {
    setTimeout(() => {
      this.dbAPI.next(this.db[gameName].find(item => item.id === id));
    }, 500)
  }


}
