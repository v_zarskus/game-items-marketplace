# GameItemsMarketplace

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Instructions

If you do not have git installed on your computer, please follow the instructions on this website:  
https://www.atlassian.com/git/tutorials/install-git  

If you do not have npm installed on your computer, please follow the instructions on this website:  
https://www.npmjs.com/get-npm  

To start the application, please run these commands in your command line:  
    _git clone https://v_zarskus@bitbucket.org/v_zarskus/game-items-marketplace.git_  
    _cd game-items-marketplace_  
    _npm install_  
    _ng serve_  

Once the application has succesfully compiled, please navigate to `http://localhost:4200/`.  

### Additional notes  
Biggest variation of items and different filters are for the "Rocket League" game, so I recommend checking it out the most.  

